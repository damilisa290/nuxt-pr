export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "nuxt-pr",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/logo1.png" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "element-ui/lib/theme-chalk/index.css",
    { src: `./assets/styles/index.scss`, lang: "scss" },
  ],
  plugins: [
    // ssr: true означает, что этот плагин работает только на сервере
    { src: "~/plugins/Elementui", ssr: true },
  ],
  build: {
    vendor: ["element-ui"],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,
  loading: {
    color: "blue",
  },
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/style-resources", "@nuxtjs/axios"],
  styleResources: {
    scss: ["~/assets/styles/*.scss"],
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  router: {
    prefetchLinks: false,
  },
};
