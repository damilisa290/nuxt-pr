export const state = () => ({
	clients: [],
});

export const mutations = {
	SET_CLIENTS(state, clients) {
		state.clients = clients;
	},
};

export const actions = {
	async GET_CLIENT({ commit }) {
		const clients = await this.$axios
			.$get("https://jsonplaceholder.typicode.com/todos")
		commit("SET_CLIENTS", clients);
	},
};
