export const state = () => ({
  token: false,
});

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token;
  },
};

export const actions = {
  login({ commit }) {
    commit("SET_TOKEN", true);
  },
  logout({ commit }) {
    commit("SET_TOKEN", false);
  },
};
