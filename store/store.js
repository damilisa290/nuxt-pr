//import axios from "axios";

export const state = () => ({
  users: [],
  currentUser: {},
});

export const mutations = {
  SET_USERS(state, users) {
    state.users = users;
  },
  SET_CURRENT_USER(state, user) {
    state.currentUser = user;
  },
};

export const actions = {
  async GET_USERS({ commit }) {
    const users = await this.$axios.$get(
      "https://jsonplaceholder.typicode.com/users"
    );
    //.then((response) => response.data);
    commit("SET_USERS", users);
  },
  async GET_CURRENT_USER({ commit }, id) {
    const user = await this.$axios.$get(
      "https://jsonplaceholder.typicode.com/users/" + id
    );
    //.then((response) => response.data);
    commit("SET_CURRENT_USER", user);
  },
};
